/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imaginamos.service.impl;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.imaginamos.model.dao.IEvidenciaUsuarioDAO;
import com.imaginamos.model.dao.IUsuarioDAO;
import com.imaginamos.model.dao.impl.EvidenciaUsuarioDAOImpl;
import com.imaginamos.model.dao.impl.UsuarioDAOImpl;
import com.imaginamos.model.pojo.EvidenciaUsuario;
import com.imaginamos.model.pojo.Usuario;
import com.imaginamos.model.service.IService;

import javax.ws.rs.Path;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.jboss.resteasy.annotations.Body;

/**
 *
 * @author santi
 */
@Path("service")
public class ServicioImpl implements IService {

    IUsuarioDAO usuarioDAO = new UsuarioDAOImpl();

    public ServicioImpl() {

    }

    @Override
    @POST
    @Path("/crearUsuario")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response fnCrearUsuario(String json) {
        JsonParser parser = new JsonParser();
        JsonElement elementObject = parser.parse(json);
        Usuario usuario = new Usuario();

        usuario.setNombre(elementObject.getAsJsonObject().get("nombre").getAsString());
        usuario.setApellido(elementObject.getAsJsonObject().get("apellido").getAsString());
        usuario.setDocumento(elementObject.getAsJsonObject().get("documento").getAsString());
        usuario.setCargo(elementObject.getAsJsonObject().get("cargo").getAsString());
        usuario.setSalario(elementObject.getAsJsonObject().get("salario").getAsString());
        usuario.setImagen(elementObject.getAsJsonObject().get("imagen").getAsString());
        usuarioDAO.fnCrearEncargadoBodega(usuario);
        return Response.status(201).entity("Servicio OK " + json).build();
    }
    
    
    @POST
    @Path("/crearUsuarios")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response fnCrearUsuarios(String json) {
        JsonParser parser = new JsonParser();
        JsonElement elementObject = parser.parse(json);
        
        JsonArray  array = elementObject.getAsJsonArray();
        
        for(JsonElement j: array){
            Usuario usuario = new Usuario();
            usuario.setNombre(j.getAsJsonObject().get("nombre").getAsString());
            usuario.setApellido(j.getAsJsonObject().get("apellido").getAsString());
            usuario.setDocumento(j.getAsJsonObject().get("documento").getAsString());
            usuario.setCargo(j.getAsJsonObject().get("cargo").getAsString());
            usuario.setSalario(j.getAsJsonObject().get("salario").getAsString());
            usuario.setImagen(j.getAsJsonObject().get("imagen").getAsString());
            usuarioDAO.fnCrearEncargadoBodega(usuario);        
        }        
        
        return Response.status(201).entity(json).build();
    }
    

    @GET
    @Path("/getusuario/{json}")
    @Produces(MediaType.TEXT_PLAIN + ";charset=utf-8")
    @Override
    public Usuario fnConsultarUsuario(@PathParam("json") String js) {
        JsonParser parser = new JsonParser();
        JsonElement elementObject = parser.parse(js);
        String id = elementObject.getAsJsonObject().get("id").getAsString();

        Usuario u = usuarioDAO.buscarUser(Integer.parseInt(id));
        return u;
    }

    @Override
    @POST
    @Path("/crearUsuario")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response fnCrearEvidencia(String json) {
        JsonParser parser = new JsonParser();
        JsonElement elementObject = parser.parse(json);
        EvidenciaUsuario EVusuario = new EvidenciaUsuario();
        IUsuarioDAO usuarioDAO = new UsuarioDAOImpl();
        IEvidenciaUsuarioDAO EVusuarioDAO = new EvidenciaUsuarioDAOImpl();
        EVusuario.setNombre(elementObject.getAsJsonObject().get("nombre").getAsString());
        EVusuario.setCreado_desde(elementObject.getAsJsonObject().get("Creado_desde").getAsString());
        EVusuario.setUsuario(this.usuarioDAO.buscarUser(elementObject.getAsJsonObject().get("Usuario_id").getAsInt()));
        EVusuarioDAO.crearEvidenciaUsuario(EVusuario);
        return Response.status(201).entity("Servicio OK " + json).build();
    }

}
