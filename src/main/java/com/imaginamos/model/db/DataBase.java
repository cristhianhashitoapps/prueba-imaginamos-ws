package com.imaginamos.model.db;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.CallableStatement;

public class DataBase {

    //public final String db = "profitlinedb";
    //public final String url = "jdbc:mysql://69.164.192.224/" + db;
    //public final String user = "root";
    //public final String pass = "@119ak7VaG70l7ec";
    
    public final String db = "imaginamos";
    public final String url = "jdbc:mysql://localhost:3306/" + db;
    public final String user = "root";
    public final String pass = "";
    private static DataBase instance;

    Connection ocnx;

    public DataBase() {

    }

    /**
     * Returns an instance of the class (singleton)
     *
     * @return DataBase
     */
    public static synchronized DataBase getInstance() {
        if (instance == null) {
            instance = new DataBase();
        }
        return instance;
    }

    public Connection fnConectar() {

        Connection ocnxLocal;

        try {

            if (this.getOcnx() == null) {
                Class.forName("com.mysql.jdbc.Driver");
                ocnxLocal = (Connection) DriverManager.getConnection(this.url, this.user, this.pass);
                this.setOcnx(ocnxLocal);
            } else {

                ocnxLocal = this.getOcnx();

            }

        } catch (Exception ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, "Error en fnConectar " + ex);
            ocnxLocal = null;
        }

        return ocnxLocal;

    }

    public void mtDesconectar(Connection oConexion) {

        try {

            if (!oConexion.isClosed()) {
                oConexion.close();
            }

        } catch (Exception ex) {

            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, "Error en fnConectar " + ex);

        }

    }

    public void mtCerrarPS(PreparedStatement oPS) {

        try {

            if (oPS != null) {

                oPS.close();

            }

        } catch (Exception ex) {

            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, "Error en mtCerrarPS " + ex);

        }

    }

    public void mtCerrarRS(ResultSet oRS) {

        try {

            if (oRS != null) {

                oRS.close();

            }

        } catch (Exception ex) {

            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, "Error en mtCerrarRS " + ex);

        }

    }

    public void mtCerrarS(Statement oS) {

        try {

            if (oS != null) {

                oS.close();

            }

        } catch (Exception ex) {

            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, "Error en mtCerrarS " + ex);

        }

    }

    public void mtCerrarCS(CallableStatement oCS) {

        try {

            if (oCS != null) {

                oCS.close();

            }

        } catch (Exception ex) {

            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, "Error en mtCerrarCS " + ex);

        }

    }

    public Connection getOcnx() {
        return ocnx;
    }

    public void setOcnx(Connection ocnx) {
        this.ocnx = ocnx;
    }

}
