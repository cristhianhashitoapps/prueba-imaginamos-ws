/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imaginamos.model.service;

import com.imaginamos.model.pojo.Usuario;
import java.util.List;
import javax.ws.rs.core.Response;

/**
 *
 * @author santi
 */
public interface IService {

    public Response fnCrearUsuario(String json);

    public Usuario fnConsultarUsuario(String json);

    public Response fnCrearEvidencia(String json);

}
