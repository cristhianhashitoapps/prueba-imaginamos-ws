/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imaginamos.model.pojo;

import java.util.Date;

/**
 *
 * @author santi
 */
public class EvidenciaUsuario {
    private int id;
    private String nombre,creado_desde;
    private Date created_at,updated_at;
    private Usuario usuario;
    private String base64;

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    

    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64;
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCreado_desde() {
        return creado_desde;
    }

    public void setCreado_desde(String creado_desde) {
        this.creado_desde = creado_desde;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public Usuario getUsuario() {
        return usuario;
    }

   
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EvidenciaUsuario other = (EvidenciaUsuario) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
    
}
