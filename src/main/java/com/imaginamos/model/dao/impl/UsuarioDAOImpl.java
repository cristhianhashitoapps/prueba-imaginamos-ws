/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imaginamos.model.dao.impl;

import com.imaginamos.model.db.DataBase;
import com.imaginamos.model.pojo.Usuario;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.imaginamos.model.dao.IUsuarioDAO;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Cristiandi
 */
public class UsuarioDAOImpl implements IUsuarioDAO {


    @Override
    public Usuario buscarUser(int id) {
        
        DataBase dataBase = new DataBase();
        Connection conexionLocal = dataBase.fnConectar();
        Statement oS = null;
        ResultSet oRS = null;

        String sSQL
                = "SELECT  t1.id, "
                + "t1.nombre, "
                + "t1.apellido, "
                + "t1.documento, "
                + "t1.cargo, "
                + "t1.salario, "
                + "t1.created_at, "
                + "t1.updated_at "                
                + "FROM usuarios t1 "
                + "WHERE t1.id = '" + id + "'";

        Usuario user = new Usuario();

      
        try {

            oS = (Statement) conexionLocal.createStatement();
            oRS = oS.executeQuery(sSQL);

            if (oRS.next()) {

                user.setId(oRS.getInt("id"));
                user.setNombre(oRS.getString("nombre"));
                user.setApellido(oRS.getString("apellido"));
                user.setDocumento(oRS.getString("documento"));
                user.setCargo(oRS.getString("cargo"));
                user.setSalario(oRS.getString("salario"));
                user.setImagen(oRS.getString("imagen"));
                user.setCreatedAt(oRS.getDate("created_at"));
                user.setUpdatedAt(oRS.getDate("updated_at"));

            } else {

                return null;

            }

        } catch (SQLException ex) {

            

        } finally {

            dataBase.mtCerrarRS(oRS);
            dataBase.mtCerrarS(oS);
            dataBase.mtDesconectar(conexionLocal);

        }

        return user;
        
    }
    public Map<Integer, Usuario> fnCrearEncargadoBodega(Usuario usuario) {

        Map<Integer, Usuario> salida = new HashMap<>();

        String sSQL
                = "INSERT INTO usuarios (nombre,apellido,documento,cargo,salario,imagen)"
                + " VALUES ("
                + "'" + usuario.getNombre() + "', "
                + "'" + usuario.getApellido() + "', "
                + "'" + usuario.getDocumento() + "', "
                + "'" + usuario.getCargo() + "', "
                + "'" + usuario.getSalario() + "',"
                + "'" + usuario.getImagen()+ "'"
                + ")";

        DataBase dataBase = new DataBase();
        Connection conexionLocal = null;
        PreparedStatement oPS = null;
        ResultSet oRS = null;

        try {

            conexionLocal = dataBase.fnConectar();

            oPS = (PreparedStatement) conexionLocal.prepareStatement(sSQL, Statement.RETURN_GENERATED_KEYS);

            //Valido si se realizo la insercion
            if (oPS.executeUpdate() > 0) {

                //Obtengo el id generado
                int ultimoId;
                oRS = oPS.getGeneratedKeys();

                if (!oRS.next()) {
                    salida.put(-1, null);
                }

                //Seteo el id al objeto
                ultimoId = oRS.getInt(1);

                usuario.setId(ultimoId);

                salida.put(0, usuario);

            } else {
                salida.put(-1, null);
            }

        } catch (SQLException ex) {

            salida.put(-1, null);
            Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, "Error en fnCrearEncargado: {0}", ex);

        } finally {

            dataBase.mtCerrarPS(oPS);
            dataBase.mtCerrarRS(oRS);
            dataBase.mtDesconectar(conexionLocal);

        }

        return salida;

    }

}
