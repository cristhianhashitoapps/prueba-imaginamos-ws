/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imaginamos.model.dao.impl;



import com.imaginamos.model.db.DataBase;
import com.imaginamos.model.pojo.EvidenciaUsuario;
import com.imaginamos.util.Utils;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.imageio.ImageIO;
import com.imaginamos.model.dao.IEvidenciaUsuarioDAO;
import com.imaginamos.model.dao.IUsuarioDAO;

/**
 *
 * @author santi
 */
public class EvidenciaUsuarioDAOImpl implements IEvidenciaUsuarioDAO {

    IUsuarioDAO iUsuarioDAO;

    @Override
    public EvidenciaUsuario buscarEvidenciaDocumento(int id) {

        DataBase dataBase = new DataBase();
        Connection conexionLocal = dataBase.fnConectar();
        Statement oS = null;
        ResultSet oRS = null;

        String sSQL
                = "SELECT t1.id, "
                + "t1.nombre, "
                + "t1.usuarios_id, "
                + "t1.creado_desde, "
                + "t1.created_at, "
                + "t1.updated_at "
                + "FROM evidencias_usuarios t1 "
                + "WHERE t1.id = '" + id + "'";

        EvidenciaUsuario evidenciaUsuario = new EvidenciaUsuario();

        iUsuarioDAO = new UsuarioDAOImpl();

        try {

            oS = (Statement) conexionLocal.createStatement();
            oRS = oS.executeQuery(sSQL);

            if (oRS.next()) {

                evidenciaUsuario.setId(oRS.getInt("id"));
                evidenciaUsuario.setNombre(oRS.getString("nombre"));
                evidenciaUsuario.setUsuario(iUsuarioDAO.buscarUser(oRS.getInt("usuarios_id")));
                evidenciaUsuario.setCreado_desde(oRS.getString("creado_desde"));
                evidenciaUsuario.setCreated_at(oRS.getDate("created_at"));
                evidenciaUsuario.setUpdated_at(oRS.getDate("updated_at"));

                return evidenciaUsuario;

            } else {

                
                return null;

            }

        } catch (SQLException ex) {

            
            return null;

        } finally {

            dataBase.mtCerrarRS(oRS);
            dataBase.mtCerrarS(oS);
            dataBase.mtDesconectar(conexionLocal);

        }

    }

    @Override
    public Map<Integer, EvidenciaUsuario> crearEvidenciaUsuario(EvidenciaUsuario evidenciaUsuario) {
        Map<Integer, EvidenciaUsuario> salida = new HashMap<>();

        String sSQL
                = "INSERT INTO evidencias_usuarios ( "
                + "nombre, "
                + "creado_desde, "
                + "usuario_id "
                + ") "
                + "VALUES ( "
                + "'" + evidenciaUsuario.getUsuario().getNombre() + "', "
                + "'" + evidenciaUsuario.getCreado_desde() + "',"
                + "'" + Utils.fnFomatearFechaTiempo(new Date()) + "' "
                + ")";

        DataBase dataBase = new DataBase();
        Connection conexionLocal = null;
        PreparedStatement oPS = null;
        ResultSet oRS = null;

        try {

            conexionLocal = dataBase.fnConectar();

            oPS = (PreparedStatement) conexionLocal.prepareStatement(sSQL, Statement.RETURN_GENERATED_KEYS);

            //Valido si se realizo la insercion
            if (oPS.executeUpdate() > 0) {

                //Obtengo el id generado
                int ultimoId;
                oRS = oPS.getGeneratedKeys();

                if (!oRS.next()) {
                    salida.put(-1, null);
                    return salida;
                }

                //Seteo el id al objeto
                ultimoId = oRS.getInt(1);

                evidenciaUsuario.setId(ultimoId);

                //Creo la imagen
                String nombreImagen = this.crearImagenEvidencia(evidenciaUsuario);

                if (nombreImagen == null) {

                    salida.put(-1, null);

                    return salida;

                }

                evidenciaUsuario.setNombre(nombreImagen);

                //Actualizo el registro seteando el nombre de la imagen creada 
                sSQL
                        = "UPDATE evidencias_pagos SET "
                        + "nombre = '" + evidenciaUsuario.getNombre() + "' "
                        + "WHERE id = '" + evidenciaUsuario.getId() + "'";

                oPS = (PreparedStatement) conexionLocal.prepareStatement(sSQL);

                if (!(oPS.executeUpdate() > 0)) {

                    salida.put(-1, null);

                    return salida;

                }

                salida.put(0, evidenciaUsuario);

                return salida;

            } else {

                salida.put(-1, null);

                return salida;

            }

        } catch (SQLException ex) {

            salida.put(-1, null);
            
            return salida;

        } finally {

            dataBase.mtCerrarPS(oPS);
            dataBase.mtCerrarRS(oRS);
            dataBase.mtDesconectar(conexionLocal);

        }

    }

    private String crearImagenEvidencia(EvidenciaUsuario evidenciaDocumento) {

        File file = new File("C:/Repositorios/imaginamos/ServiciosWeb/imaginamosarchivos/evidencias/usuario");

        //Se valida si existe los directorios para los flujos alternos
        if (!file.exists()) {

            file.mkdirs();

        }

        String nombreFlujoAlterno = "evidenciaUsuario_" + evidenciaDocumento.getUsuario().getId();

        file = new File(file.getAbsolutePath() + "//" + nombreFlujoAlterno);

        //Se valida si existe la carpeta el flujo alterno
        if (!file.exists()) {

            file.mkdirs();

        }

        ByteArrayInputStream bis = null;

        try {

            // create a buffered image
            BufferedImage image;
            byte[] base64decodedBytes = Utils.fnDecodeBase64(evidenciaDocumento.getBase64());

            bis = new ByteArrayInputStream(base64decodedBytes);

            image = ImageIO.read(bis);

            String nombreEvidencia = "evidencia_" + evidenciaDocumento.getId() + ".png";

            file = new File(file.getAbsolutePath() + "//" + nombreEvidencia);

            boolean write = ImageIO.write(image, "png", file);

            if (!write) {
                return null;
            }

            return file.getName();

        } catch (IOException ex) {

        
            return null;

        } finally {

            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException ex) {
                  
                }
            }

        }

    }

}
