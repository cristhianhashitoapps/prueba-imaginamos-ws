/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imaginamos.model.dao;

import com.imaginamos.model.pojo.EvidenciaUsuario;
import java.util.Map;

/**
 *
 * @author santi
 */
public interface IEvidenciaUsuarioDAO {
    public EvidenciaUsuario buscarEvidenciaDocumento(int id);
    Map<Integer, EvidenciaUsuario> crearEvidenciaUsuario(EvidenciaUsuario evidenciaDocumento);
    
}
