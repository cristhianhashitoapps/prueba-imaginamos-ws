/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imaginamos.util;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Cristiandi
 */
public class SendMailTLS {

    private static final String SMTP_HOST_NAME = "smtp.gmail.com";
    private static final String SMTP_AUTH_USER = "cristian.ippolito@imaginamos.com.co";
    private static final String SMTP_AUTH_PWD = "cristiandi@imaginamos";    

    /**
     *
     * @param detinatarios personas a las que se les envia el correo
     * @param asunto asunto del correo
     * @param mensaje contenido del correo
     * @param remitente persona quien envia el correo
     * @return
     */
    public boolean fnEnviarCorreo (String detinatarios[], String asunto, String mensaje, String remitente) {
        
        //Si no envian remitente utiliza el configurado en la clase
        if(remitente == null){
            remitente = SMTP_AUTH_USER;
        }
        
        boolean debug = false;
        Session session;

        Properties props = new Properties();

        props.setProperty("mail.smtp.host", SMTP_HOST_NAME);
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.port", "" + 587);
        props.setProperty("mail.smtp.starttls.enable", "true");
        // props.setProperty("mail.debug", "true");

        session = Session.getInstance(props, new javax.mail.Authenticator() {

            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(SMTP_AUTH_USER, SMTP_AUTH_PWD);
            }
        });

        session.setDebug(debug);

        // Creo el mensaje
        Message msg = new MimeMessage(session);

        try {

            // creo el addressFrom apartir del remitente 
            InternetAddress addressFrom = new InternetAddress(remitente);
            msg.setFrom(addressFrom);

            InternetAddress[] addressTo = new InternetAddress[detinatarios.length];
            for (int i = 0; i < detinatarios.length; i++) {
                addressTo[i] = new InternetAddress(detinatarios[i]);
            }
            msg.setRecipients(Message.RecipientType.TO, addressTo);

            // Seteo el asunto, mensaje y el tipo de contenido
            msg.setSubject(asunto);
            //msg.setContent(mensaje, "text/plain");
            msg.setContent(mensaje, "text/html; charset=utf-8");
            Transport.send(msg);
            
            return true;

        } catch (MessagingException ex) {

            Logger.getLogger(SendMailTLS.class.getName()).log(Level.SEVERE, "Error en fnEnviarCorreo: {0}", ex);
            return false;

        }

    }

}
