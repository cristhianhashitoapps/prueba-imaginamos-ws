package com.imaginamos.util;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.mindrot.jbcrypt.BCrypt;

public class Utils {

    public static String fnFomatearFecha(Date fecha) {

	DateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
	return formatter.format(fecha);

    }

    public static String fnFomatearFechaTiempo(Date fecha) {
	
	if(fecha == null){
	    return null;
	}
	
	DateFormat formatter = new SimpleDateFormat("YYY-MM-dd hh:mm:ss");
	return formatter.format(fecha);

    }

    public static Date fnObtenerDate(String fecha) {

	DateFormat formatter = new SimpleDateFormat("dd/mm/yyyy");
	java.util.Date javaDate;
	try {
	    javaDate = formatter.parse(fecha);
	} catch (ParseException e) {
	    e.printStackTrace();
	    javaDate = null;
	}
	return javaDate;
    }

    public static Date fnObtenerDateTime(String dateTime) {

	DateFormat formatter = new SimpleDateFormat("YYY-MM-dd hh:mm:ss");
	java.util.Date javaDate;
	try {
	    javaDate = formatter.parse(dateTime);
	} catch (ParseException e) {
	    e.printStackTrace();
	    javaDate = null;
	}
	return javaDate;
    }

    /**
     * funcion encargada de devolver un texto encryptado compatible con JBCrypt
     * para que se pueda validar
     *
     * @param encryptedText texto encryptado por PHP
     * @return texto encriptado compatible para la validacion con la clase
     * JBCrypt
     */
    public static String fnAdaptEncryptPHPForJAVA(String encryptedText) {

	return encryptedText.replaceFirst("2y", "2a");

    }

    public static String fnEncryptJavaForPHP(String texto) {

	return BCrypt.hashpw(texto, BCrypt.gensalt()).replaceFirst("2a", "2y");

    }

    public static String fnGenerateRandomString() {

	String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	StringBuilder salt = new StringBuilder();
	Random rnd = new Random();
	while (salt.length() < 18) { // length of the random string.
	    int index = (int) (rnd.nextFloat() * SALTCHARS.length());
	    salt.append(SALTCHARS.charAt(index));
	}
	String saltStr = salt.toString();
	return saltStr;

    }

    /**
     * Funcion para decodificar un base 65, te devuelve un arreglo de bytes
     *
     * @param encodeBase64
     * @return
     */
    public static byte[] fnDecodeBase64(String encodeBase64) {

	return Base64.getDecoder().decode(encodeBase64);

    }

    /**
     * Funcion para devolver el JSON que genera un objeto
     *
     * @param object
     * @return
     */
    public static String fnObjetToJson(Object object) {

	ObjectMapper mapper = new ObjectMapper();

	try {

	    String jsonInString = mapper.writeValueAsString(object);
	    return jsonInString;

	} catch (IOException ex) {

	    Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, "Error en fnObjetToJson: {0}", ex);
	    return null;

	}

    }

    /**
     *
     * @param value
     */
    public static void mtWriteString(String value) {

	BufferedWriter bw = null; 
	try {
	    
	    bw = new BufferedWriter(new FileWriter("JSON.txt"));
	    
	    bw.write(value);  
	    
	} catch (IOException ex) {
	    
	    Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, "Error en mtWriteString: {0}", ex);

	}	
	finally{
	    
	    try {
		if(bw != null){
		    bw.close();
		}		
	    } catch (IOException ex) {
		Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, "Error en mtWriteString cerrando bw: {0}", ex);
	    }
	    
	}

    }   
   
}
